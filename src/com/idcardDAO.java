package com;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class idcardDAO implements IidcardDAO{
	protected static final String FIELDS_INSERT="sign,id,name,sex,email,recycle";
	protected static String INSERT_SQL="insert into idcard("+FIELDS_INSERT+") values(?,?,?,?,?,?)";
	protected static String SELECT_SQL="select * from idcard where sign=? and id=? and recycle=0  order by id";
	protected static String UPDATE_SQL="update idcard set name=?,sex=?,email=?,recycle=? where id=? and sign=?";
	protected static String DELETE_SQL="delete from idcard where id=? and sign=?";
	protected static String UPDATE_SQL02="update idcard set recycle=1 where sign=? and id=? and recycle=0";
	
	@Override
	public int insert(idcard01 idcard01) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		ResultSet rs=null;
		PreparedStatement pst=conn.prepareStatement(INSERT_SQL);
		pst.setInt(1, idcard01.getSign());
		pst.setInt(2, idcard01.getId());
		pst.setString(3, idcard01.getName());
		pst.setString(4, idcard01.getSex());
		pst.setString(5, idcard01.getEmail());
		pst.setInt(6, 0);
		int n=pst.executeUpdate();
		dbc.closeDB(conn, pst, rs);
		return n;
	}

	@Override
	public int delete(int id,int sign) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		ResultSet rs=null;
		PreparedStatement pst=conn.prepareStatement(DELETE_SQL);
		pst.setInt(1, id);
		pst.setInt(1, sign);
		int n=pst.executeUpdate();
		dbc.closeDB(conn, pst, rs);
		return n;
	}

	@Override
	public boolean query(int sign,int id) throws Exception {
		boolean flag=false;
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		PreparedStatement pst=conn.prepareStatement(SELECT_SQL);
		pst.setInt(1, sign);
		pst.setInt(2, id);
		ResultSet rs=pst.executeQuery();
		idcard01 idcard02=null;
		if(rs.next())
		{
			idcard02=new idcard01();
			idcard02.setSign(rs.getInt("sign"));
			idcard02.setId(rs.getInt("id"));
			idcard02.setName(rs.getString("name"));
			idcard02.setSex(rs.getString("sex"));
			idcard02.setEmail(rs.getString("email"));
			idcard02.setRecycle(rs.getInt("recycle"));
			flag=true;
		}
		dbc.closeDB(conn, pst, rs);
		return flag;
	}

	@Override
	public List<idcard01> queryALL(int sign) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<idcard01> card=new ArrayList<idcard01>();
		PreparedStatement pst=conn.prepareStatement("select * from idcard where sign=? and recycle=0 order by id");
		pst.setInt(1, sign);
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			idcard01 idcard02=new idcard01();
			idcard02.setSign(rs.getInt("sign"));
			idcard02.setId(rs.getInt("id"));
			idcard02.setName(rs.getString("name"));
			idcard02.setSex(rs.getString("sex"));
			idcard02.setEmail(rs.getString("email"));
			idcard02.setRecycle(rs.getInt("recycle"));
			card.add(idcard02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}

	@Override
	public int update01(com.idcard01 idcard01) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		ResultSet rs=null;
		PreparedStatement pst=conn.prepareStatement(UPDATE_SQL);
		pst.setString(1, idcard01.getName());
		pst.setString(2, idcard01.getSex());
		pst.setString(3, idcard01.getEmail());
		pst.setInt(4, idcard01.getRecycle());
		pst.setInt(5, idcard01.getId());
		pst.setInt(6, idcard01.getSign());
		int n=pst.executeUpdate();
		dbc.closeDB(conn, pst, rs);
		return n;
	}

	@Override
	public int update02(int sign,int id) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		ResultSet rs=null;
		PreparedStatement pst=conn.prepareStatement(UPDATE_SQL02);
		pst.setInt(1, sign);
		pst.setInt(2, id);
		int n=pst.executeUpdate();
		dbc.closeDB(conn, pst, rs);
		return n;
	}
	
	@Override
	public List<idcard01> query_ID(int sign,String id) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<idcard01> card=new ArrayList<idcard01>();
		String sql1="select * from idcard where id like '%";
		String sql2=id;
		String sql3="%' and sign=? and recycle=0 order by id";
		String query_ID=sql1+sql2+sql3;
		PreparedStatement pst=conn.prepareStatement(query_ID);
		pst.setInt(1, sign);
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			idcard01 idcard02=new idcard01();
			idcard02.setSign(rs.getInt("sign"));
			idcard02.setId(rs.getInt("id"));
			idcard02.setName(rs.getString("name"));
			idcard02.setSex(rs.getString("sex"));
			idcard02.setEmail(rs.getString("email"));
			idcard02.setRecycle(rs.getInt("recycle"));
			card.add(idcard02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}

	@Override
	public List<idcard01> query_NAME(int sign,String name) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<idcard01> card=new ArrayList<idcard01>();
		String sql1="select * from idcard where name like '%";
		String sql2=name;
		String sql3="%' and sign=? and recycle=0 order by id";
		String query_NAME=sql1+sql2+sql3;
		PreparedStatement pst=conn.prepareStatement(query_NAME);
		pst.setInt(1, sign);
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			idcard01 idcard02=new idcard01();
			idcard02.setSign(rs.getInt("sign"));
			idcard02.setId(rs.getInt("id"));
			idcard02.setName(rs.getString("name"));
			idcard02.setSex(rs.getString("sex"));
			idcard02.setEmail(rs.getString("email"));
			idcard02.setRecycle(rs.getInt("recycle"));
			card.add(idcard02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}

	@Override
	public List<idcard01> recycle_query_ID(int sign, String id) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<idcard01> card=new ArrayList<idcard01>();
		String sql1="select * from idcard where id like '%";
		String sql2=id;
		String sql3="%' and sign=? and recycle=1 order by id";
		String recycle_query_ID=sql1+sql2+sql3;
		PreparedStatement pst=conn.prepareStatement(recycle_query_ID);
		pst.setInt(1, sign);
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			idcard01 idcard02=new idcard01();
			idcard02.setSign(rs.getInt("sign"));
			idcard02.setId(rs.getInt("id"));
			idcard02.setName(rs.getString("name"));
			idcard02.setSex(rs.getString("sex"));
			idcard02.setEmail(rs.getString("email"));
			idcard02.setRecycle(rs.getInt("recycle"));
			card.add(idcard02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}

	@Override
	public List<idcard01> recycle_query_NAME(int sign, String name) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<idcard01> card=new ArrayList<idcard01>();
		String sql1="select * from idcard where name like '%";
		String sql2=name;
		String sql3="%' and sign=? and recycle=1 order by id";
		String recycle_query_NAME=sql1+sql2+sql3;
		PreparedStatement pst=conn.prepareStatement(recycle_query_NAME);
		pst.setInt(1, sign);
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			idcard01 idcard02=new idcard01();
			idcard02.setSign(rs.getInt("sign"));
			idcard02.setId(rs.getInt("id"));
			idcard02.setName(rs.getString("name"));
			idcard02.setSex(rs.getString("sex"));
			idcard02.setEmail(rs.getString("email"));
			idcard02.setRecycle(rs.getInt("recycle"));
			card.add(idcard02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}

	@Override
	public List<idcard01> recycle_query_ALL(int sign) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<idcard01> card=new ArrayList<idcard01>();
		String recycle_query_ALL="select * from idcard where sign=? and recycle=1 order by id";
		PreparedStatement pst=conn.prepareStatement(recycle_query_ALL);
		pst.setInt(1, sign);
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			idcard01 idcard02=new idcard01();
			idcard02.setSign(rs.getInt("sign"));
			idcard02.setId(rs.getInt("id"));
			idcard02.setName(rs.getString("name"));
			idcard02.setSex(rs.getString("sex"));
			idcard02.setEmail(rs.getString("email"));
			idcard02.setRecycle(rs.getInt("recycle"));
			card.add(idcard02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}

	@Override
	public int recycle_delete_ALL(int sign) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		ResultSet rs=null;
		String recycle_delete_ALL="delete from idcard where sign=? and recycle=1";
		PreparedStatement pst=conn.prepareStatement(recycle_delete_ALL);
		pst.setInt(1, sign);
		int n=pst.executeUpdate();
		dbc.closeDB(conn, pst, rs);
		return n;
	}

	@Override
	public int recycle_reduction_ALL(int sign) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		ResultSet rs=null;
		String recycle_reduction_ALL="update idcard set recycle=0 where sign=? and recycle=1";
		PreparedStatement pst=conn.prepareStatement(recycle_reduction_ALL);
		pst.setInt(1, sign);
		int n=pst.executeUpdate();
		dbc.closeDB(conn, pst, rs);
		return n;
	}

	@Override
	public int recycle_delete_Single(int sign, int id) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		ResultSet rs=null;
		String recycle_delete_Single="delete from idcard where sign=? and id=? and recycle=1";
		PreparedStatement pst=conn.prepareStatement(recycle_delete_Single);
		pst.setInt(1, sign);
		pst.setInt(2, id);
		int n=pst.executeUpdate();
		dbc.closeDB(conn, pst, rs);
		return n;
	}

	@Override
	public int recycle_reduction_Single(int sign, int id) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		ResultSet rs=null;
		String recycle_reduction_Single="update idcard set recycle=0 where sign=? and id=? and recycle=1";
		PreparedStatement pst=conn.prepareStatement(recycle_reduction_Single);
		pst.setInt(1, sign);
		pst.setInt(2, id);
		int n=pst.executeUpdate();
		dbc.closeDB(conn, pst, rs);
		return n;
	}
	
	public String get_name(int sign) throws Exception
	{
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		String sql="select * from iduser where sign=?";
		PreparedStatement pst=conn.prepareStatement(sql);
		pst.setInt(1, sign);
		ResultSet rs=pst.executeQuery();
		String name=null;
		if(rs.next())
		{
			name=rs.getString("name");
		}
		dbc.closeDB(conn, pst, rs);
		return name;
	}
}
