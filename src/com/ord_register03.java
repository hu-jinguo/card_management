package com;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class ord_register03 {
	private int sign;
	private String name;
	private String root;
	private String password;
	private int recycle=0;
	public int getSign() {
		return sign;
	}
	public void setSign(int sign) {
		this.sign = sign;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRoot() {
		return root;
	}
	public void setRoot(String root) {
		this.root = root;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getRecycle() {
		return recycle;
	}
	public void setRecycle(int recycle) {
		this.recycle = recycle;
	}
	public ord_register03(){}
	public ord_register03(int sign01,String name01,String root01,String password01)
	{
		sign=sign01;
		name=name01;
		root=root01;
		password=password01;
		recycle=0;
	}
	public int pdUser(int sign01,String name01,String root01,String password01) throws Exception
	{
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		String sql="insert into iduser(sign,name,root,password,recycle) values(?,?,?,?,?)";
		PreparedStatement pst=conn.prepareStatement(sql);
		pst.setInt(1, sign01);
		pst.setString(2, name01);
		pst.setString(3, root01);
		pst.setString(4, password01);
		pst.setInt(5, 0);
		int n=pst.executeUpdate();
		return n;
	}
}
