package com;  
  
import java.io.File;  
import java.util.List;  
import com.idcard01;  
import jxl.Workbook;  
import jxl.write.Label;  
import jxl.write.WritableSheet;  
import jxl.write.WritableWorkbook;  
  
public class Excel01 {  
    public void excleOut(List<idcard01> list,String name) throws Exception {  
    	String path="d:/"+name+"的名片.xls";
    	WritableWorkbook book = null;  
        // 创建一个excle对象  
        book = Workbook.createWorkbook(new File(path));  
        // 通过excle对象创建一个选项卡对象  
        WritableSheet sheet = book.createSheet("idcard01", 0);  
        // 创建一个单元格对象 列 行 值  
        // Label label = new Label(0, 2, "test");
        Label label=null;
        String[] title={"标志号","学号","姓名","性别","邮箱"};
        for (int i=0;i<title.length;i++){
            label=new Label(i,0,title[i]);
            sheet.addCell(label);
        }
        int i=1;
        for (idcard01 nc:list) {  
            Label label1 = new Label(0, i, Integer.toString(nc.getSign()));  
            Label label2 = new Label(1, i, Integer.toString(nc.getId()));  
            Label label3 = new Label(2, i, nc.getName());
            Label label4 = new Label(3, i, nc.getSex());
            Label label5 = new Label(4, i, nc.getEmail());

            // 将创建好的单元格对象放入选项卡中  
            sheet.addCell(label1);
            sheet.addCell(label2);  
            sheet.addCell(label3);
            sheet.addCell(label4);
            sheet.addCell(label5);
            i++;
        }  
        // 写入目标路径  
        book.write();
        book.close();
    }  
}  