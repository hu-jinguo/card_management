package com;
import java.sql.*;

public class ordUser {
	private String root=null;
	private String password=null;
	private int sign;
	public String getRoot() {
		return root;
	}
	public void setRoot(String root) {
		this.root = root;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getSign() {
		return sign;
	}
	public void setSign(int sign) {
		this.sign = sign;
	}
	
	public ordUser(){}
	public ordUser(String root1,String password1)
	{
		root=root1;
		password=password1;
	}
	public boolean pdUser(String root2,String password2) throws Exception
	{
		boolean flag=false;
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		String sql="select * from iduser where root=? and password=? and recycle=0";
		PreparedStatement pst=conn.prepareStatement(sql);
		pst.setString(1, root2);
		pst.setString(2, password2);
		ResultSet rs=pst.executeQuery();
		if(rs.next())
		{
			sign=rs.getInt("sign");
			flag=true;
		}
		else flag=false;
		dbc.closeDB(conn, pst, rs);
		return flag;
	}
}
