package com;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.DBconnect;

public class ord_register01 {
	private String root=null;
	private String name=null;
	public String getRoot() {
		return root;
	}
	public void setRoot(String root) {
		this.root = root;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ord_register01(){}
	public ord_register01(String name1,String root1)
	{
		name=name1;
		root=root1;
	}
	public boolean pdUser(String name2,String root2) throws Exception
	{
		boolean flag=false;
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		String sql="select * from iduser where name=? or root=?";
		PreparedStatement pst=conn.prepareStatement(sql);
		pst.setString(1, name2);
		pst.setString(2, root2);
		ResultSet rs=pst.executeQuery();
		if(rs.next())
		{
			flag=true;
		}
		else flag=false;
		dbc.closeDB(conn, pst, rs);
		return flag;
	}
}
