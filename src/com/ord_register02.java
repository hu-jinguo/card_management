package com;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.DBconnect;

public class ord_register02 {
	private int sign;
	public int getSign() {
		return sign;
	}

	public void setSign(int sign) {
		this.sign = sign;
	}
	public ord_register02(){}
	public ord_register02(int sign01)
	{
		sign=sign01;
	}
	public boolean pdUser(int sign01) throws Exception
	{
		boolean flag=false;
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		String sqlx="select * from iduser where sign=?";
		PreparedStatement pstx=conn.prepareStatement(sqlx);
		pstx.setInt(1, sign01);
		ResultSet rsx=pstx.executeQuery();
		if(rsx.next())
		{
			flag=true;
		}
		else flag=false;
		dbc.closeDB(conn, pstx, rsx);
		return flag;
	}
}
