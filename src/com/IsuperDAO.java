package com;

import java.util.List;

import com.idcard01;

public interface IsuperDAO {
	public abstract user01 query(String name) throws Exception;//管理员查询用户
	public abstract List<user01> query_ALL() throws Exception;//管理员查询全部用户
	public abstract int update01(int sign) throws Exception;//管理员把名片表放回收站
	public abstract int update02(int sign) throws Exception;//管理员把用户放回收站
	public abstract List<idcard01> query_NAME(String name) throws Exception;//管理员查询name记录
	public abstract List<idcard01> query_ALL02() throws Exception;//管理员查询全部名片
	public abstract List<idcard01> crad_query_ALL() throws Exception;//管理员回收站查询全部名片
	public abstract List<user01> user_query_ALL() throws Exception;//管理员回收站查询全部名片
	public abstract List<idcard01> crad_query_Single(int sign,int id) throws Exception;//管理员回收站查询单个名片
	public abstract List<user01> user_query_Single(String name) throws Exception;//管理员回收站查询单个名片
}