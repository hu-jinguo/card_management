/*
Navicat MySQL Data Transfer

Source Server         : 5.7
Source Server Version : 50734
Source Host           : localhost:13306
Source Database       : card_management

Target Server Type    : MYSQL
Target Server Version : 50734
File Encoding         : 65001

Date: 2022-07-11 17:33:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for idcard
-- ----------------------------
DROP TABLE IF EXISTS `idcard`;
CREATE TABLE `idcard` (
  `sign` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `recycle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of idcard
-- ----------------------------
INSERT INTO `idcard` VALUES ('715', '1', '张三三三', '男', '123@qq.com', '0');
INSERT INTO `idcard` VALUES ('715', '3', '李四', '男', '456@qq.com', '0');
INSERT INTO `idcard` VALUES ('63', '1', '张三三', '男', '789@qq.com', '0');
INSERT INTO `idcard` VALUES ('63', '2', '蓝蓝路', '女', '111@qq.com', '0');
INSERT INTO `idcard` VALUES ('414', '1', '张三三', '女', '222@qq.com', '0');
INSERT INTO `idcard` VALUES ('414', '12', '王五五', '女', '333@qq.com', '0');
INSERT INTO `idcard` VALUES ('715', '22', '蓝蓝路', '女', '444@qq.com', '1');
INSERT INTO `idcard` VALUES ('351', '1', '张一', '女', '555@qq.com', '0');

-- ----------------------------
-- Table structure for iduser
-- ----------------------------
DROP TABLE IF EXISTS `iduser`;
CREATE TABLE `iduser` (
  `sign` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `root` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `recycle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of iduser
-- ----------------------------
INSERT INTO `iduser` VALUES ('715', '张三', 'zs', '123', '0');
INSERT INTO `iduser` VALUES ('63', '王五', 'ww', '123', '0');
INSERT INTO `iduser` VALUES ('414', '李四', 'ls', '123', '0');
INSERT INTO `iduser` VALUES ('351', '测试', 'test', '123', '0');

-- ----------------------------
-- Table structure for superroot
-- ----------------------------
DROP TABLE IF EXISTS `superroot`;
CREATE TABLE `superroot` (
  `name` varchar(255) NOT NULL,
  `root` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of superroot
-- ----------------------------
INSERT INTO `superroot` VALUES ('哈哈哈', 'hhh', '1234');
