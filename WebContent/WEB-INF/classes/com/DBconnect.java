package com;
import java.sql.*;

public class DBconnect {
	private static String url1="jdbc:mysql://localhost:13306/";
	private static String dbname="card_management";
	private static String url=url1+dbname;
	private static String user="root";
	private static String password="1234";
	
	public Connection getDBconnect() throws Exception
	{
		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager.getConnection(url, user, password);
	}
	
	public void closeDB(Connection conn,PreparedStatement pst,ResultSet rs)
	{
			try {
				if(rs!=null) rs.close();
				if(pst!=null) pst.close();
				if(conn!=null) conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
}
