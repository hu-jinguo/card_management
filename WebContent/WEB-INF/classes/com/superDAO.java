package com;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.user01;

public class superDAO implements IsuperDAO{

	@Override
	public user01 query(String name) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		PreparedStatement pst=conn.prepareStatement("select * from iduser where name=? and recycle=0");
		pst.setString(1, name);
		ResultSet rs=pst.executeQuery();
		user01 user02=null;
		if(rs.next())
		{
			user02=new user01();
			user02.setSign(rs.getInt("sign"));
			user02.setName(rs.getString("name"));
			user02.setRoot(rs.getString("root"));
			user02.setPassword(rs.getString("password"));
			user02.setRecycle(rs.getInt("recycle"));
		}
		dbc.closeDB(conn, pst, rs);
		return user02;
	}

	@Override
	public List<user01> query_ALL() throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<user01> card=new ArrayList<user01>();
		PreparedStatement pst=conn.prepareStatement("select * from iduser where recycle=0 order by sign");
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			user01 user02=new user01();
			user02.setSign(rs.getInt("sign"));
			user02.setName(rs.getString("name"));
			user02.setRoot(rs.getString("root"));
			user02.setPassword(rs.getString("password"));
			user02.setRecycle(rs.getInt("recycle"));
			card.add(user02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}

	@Override
	public int update01(int sign) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		ResultSet rs=null;
		PreparedStatement pst=conn.prepareStatement("update idcard set recycle=1 where sign=?");
		pst.setInt(1, sign);
		int n=pst.executeUpdate();
		dbc.closeDB(conn, pst, rs);
		return n;
	}

	@Override
	public int update02(int sign) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		ResultSet rs=null;
		PreparedStatement pst=conn.prepareStatement("update iduser set recycle=1 where sign=? and recycle=0");
		pst.setInt(1, sign);
		int n=pst.executeUpdate();
		dbc.closeDB(conn, pst, rs);
		return n;
	}

	@Override
	public List<idcard01> query_NAME(String name) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<idcard01> card=new ArrayList<idcard01>();
		String sql1="select * from idcard where name like '%";
		String sql2=name;
		String sql3="%' and recycle=0 order by sign,id";
		String sql=sql1+sql2+sql3;
		PreparedStatement pst=conn.prepareStatement(sql);
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			idcard01 idcard02=new idcard01();
			idcard02.setSign(rs.getInt("sign"));
			idcard02.setId(rs.getInt("id"));
			idcard02.setName(rs.getString("name"));
			idcard02.setSex(rs.getString("sex"));
			idcard02.setEmail(rs.getString("email"));
			idcard02.setRecycle(rs.getInt("recycle"));
			card.add(idcard02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}

	@Override
	public List<idcard01> query_ALL02() throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<idcard01> card=new ArrayList<idcard01>();
		PreparedStatement pst=conn.prepareStatement("select * from idcard where recycle=0 order by sign,id");
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			idcard01 idcard02=new idcard01();
			idcard02.setSign(rs.getInt("sign"));
			idcard02.setId(rs.getInt("id"));
			idcard02.setName(rs.getString("name"));
			idcard02.setSex(rs.getString("sex"));
			idcard02.setEmail(rs.getString("email"));
			idcard02.setRecycle(rs.getInt("recycle"));
			card.add(idcard02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}

	@Override
	public List<idcard01> crad_query_ALL() throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<idcard01> card=new ArrayList<idcard01>();
		PreparedStatement pst=conn.prepareStatement("select * from idcard where recycle=1");
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			idcard01 idcard02=new idcard01();
			idcard02.setSign(rs.getInt("sign"));
			idcard02.setId(rs.getInt("id"));
			idcard02.setName(rs.getString("name"));
			idcard02.setSex(rs.getString("sex"));
			idcard02.setEmail(rs.getString("email"));
			idcard02.setRecycle(rs.getInt("recycle"));
			card.add(idcard02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}

	@Override
	public List<user01> user_query_ALL() throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<user01> card=new ArrayList<user01>();
		PreparedStatement pst=conn.prepareStatement("select * from iduser where recycle=1");
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			user01 user02=new user01();
			user02.setSign(rs.getInt("sign"));
			user02.setName(rs.getString("name"));
			user02.setRoot(rs.getString("root"));
			user02.setPassword(rs.getString("password"));
			user02.setRecycle(rs.getInt("recycle"));
			card.add(user02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}

	@Override
	public List<idcard01> crad_query_Single(int sign01, int id01) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<idcard01> card=new ArrayList<idcard01>();
		PreparedStatement pst=conn.prepareStatement("select * from idcard where recycle=1 and id=? and sign=?");
		pst.setInt(1, id01);
		pst.setInt(2, sign01);
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			idcard01 idcard02=new idcard01();
			idcard02.setSign(rs.getInt("sign"));
			idcard02.setId(rs.getInt("id"));
			idcard02.setName(rs.getString("name"));
			idcard02.setSex(rs.getString("sex"));
			idcard02.setEmail(rs.getString("email"));
			idcard02.setRecycle(rs.getInt("recycle"));
			card.add(idcard02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}

	@Override
	public List<user01> user_query_Single(String name) throws Exception {
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		List<user01> card=new ArrayList<user01>();
		PreparedStatement pst=conn.prepareStatement("select * from iduser where recycle=1 and name=?");
		pst.setString(1, name);
		ResultSet rs=pst.executeQuery();
		while(rs.next())
		{
			user01 user02=new user01();
			user02.setSign(rs.getInt("sign"));
			user02.setName(rs.getString("name"));
			user02.setRoot(rs.getString("root"));
			user02.setPassword(rs.getString("password"));
			user02.setRecycle(rs.getInt("recycle"));
			card.add(user02);
		}
		dbc.closeDB(conn, pst, rs);
		return card;
	}


}
