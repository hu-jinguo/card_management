package com;

public class user01 {
	private int sign=0;
	private String name;
	private String root;
	private String password;
	private int recycle;
	
	public int getSign() {
		return sign;
	}
	public void setSign(int sign) {
		this.sign = sign;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRoot() {
		return root;
	}
	public void setRoot(String root) {
		this.root = root;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getRecycle() {
		return recycle;
	}
	public void setRecycle(int recycle) {
		this.recycle = recycle;
	}
	
	public user01(){}
	public user01(int sign01,String name01,String root01,String password01,int recycle01)
	{
		sign=sign01;
		name=name01;
		root=root01;
		password=password01;
		recycle=recycle01;
	}
}
