package com;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class super_register01 {
	private String root=null;
	private String password=null;
	public String getRoot() {
		return root;
	}
	public void setRoot(String root) {
		this.root = root;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public super_register01(){}
	public super_register01(String root1,String password1)
	{
		password=password1;
		root=root1;
	}
	public boolean pdUser(String root1,String password1) throws Exception
	{
		boolean flag=false;
		DBconnect dbc=new DBconnect();
		Connection conn=dbc.getDBconnect();
		String sql="select * from superroot where root=? and password=?";
		PreparedStatement pst=conn.prepareStatement(sql);
		pst.setString(1, root1);
		pst.setString(2, password1);
		ResultSet rs=pst.executeQuery();
		if(rs.next())
		{
			flag=true;
		}
		else flag=false;
		dbc.closeDB(conn, pst, rs);
		return flag;
	}
}
