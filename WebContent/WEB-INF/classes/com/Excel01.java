package com;  
  
import java.io.File;  
import java.io.IOException;  
import java.util.ArrayList;  
import java.util.List;  
  
import com.idcard01;  
  
import jxl.Cell;  
import jxl.Sheet;  
import jxl.Workbook;  
import jxl.write.Label;  
import jxl.write.WritableSheet;  
import jxl.write.WritableWorkbook;  
import jxl.write.WriteException;  
  
public class Excel01 {  
    public void excleOut(List<idcard01> list) {  
        WritableWorkbook book = null;  
        try {  
            // 创建一个excle对象  
            book = Workbook.createWorkbook(new File("c:/idcard.xls"));  
            // 通过excle对象创建一个选项卡对象  
            WritableSheet sheet = book.createSheet("sheet1", 0);  
            // 创建一个单元格对象 列 行 值  
            // Label label = new Label(0, 2, "test");  
            for (int i = 0; i < list.size(); i++) {  
            	idcard01 idcard01 = list.get(i);
                Label label1 = new Label(0, i, Integer.toString(idcard01.getSign()));  
                Label label2 = new Label(1, i, Integer.toString(idcard01.getId()));  
                Label label3 = new Label(2, i, idcard01.getName());
                Label label4 = new Label(3, i, idcard01.getSex());
                Label label5 = new Label(4, i, idcard01.getEmail());
                Label label6 = new Label(5, i, Integer.toString(idcard01.getRecycle()));
  
                // 将创建好的单元格对象放入选项卡中  
                sheet.addCell(label1);  
                sheet.addCell(label2);  
                sheet.addCell(label3);
                sheet.addCell(label4);  
                sheet.addCell(label5);  
                sheet.addCell(label6); 
            }  
            // 写入目标路径  
            book.write();  
        } catch (Exception e) {  
            e.printStackTrace();  
        } finally {  
            try {  
                book.close();  
            } catch (WriteException | IOException e) {  
                e.printStackTrace();  
            }  
        }  
    }  
  
    public List<idcard01> excleIn() {  
        List<idcard01> list = new ArrayList<>();  
        Workbook workbook = null;  
        try {  
            // 获取Ecle对象  
            workbook = Workbook.getWorkbook(new File("c:/idcard.xls"));  
            // 获取选项卡对象 第0个选项卡  
            Sheet sheet = workbook.getSheet(0);  
            // 循环选项卡中的值  
            for (int i = 0; i < sheet.getRows(); i++) {  
            	idcard01 idcard01 = new idcard01();  
                // 获取单元格对象  
                Cell cell0 = sheet.getCell(0, i);  
                // 取得单元格的值,并设置到对象中  
                idcard01.setSign(Integer.valueOf(cell0.getContents()));  
                // 获取单元格对象，然后取得单元格的值,并设置到对象中  
                idcard01.setId(Integer.parseInt(sheet.getCell(1, i).getContents()));  
                idcard01.setName(sheet.getCell(2, i).getContents());
                idcard01.setSex(sheet.getCell(3, i).getContents());
                idcard01.setEmail(sheet.getCell(4, i).getContents());
                idcard01.setRecycle(Integer.parseInt(sheet.getCell(5, i).getContents()));
                list.add(idcard01);  
            }  
        } catch (Exception e) {  
            e.printStackTrace();  
        } finally {  
            workbook.close();  
        }  
        return list;  
    }  
   
}  