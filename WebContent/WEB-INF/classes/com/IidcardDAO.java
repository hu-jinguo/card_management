package com;

import java.util.List;

import com.idcard01;

public interface IidcardDAO {
	public abstract int insert(idcard01 idcard01) throws Exception;//添加记录
	public abstract int delete(int id,int sign) throws Exception;//删除记录
	public abstract boolean query(int sign,int id) throws Exception;//查询记录
	public abstract List<idcard01> queryALL(int sign) throws Exception;//查询全部记录
	public abstract int update01(idcard01 idcard01) throws Exception;//修改记录01
	public abstract int update02(int sign,int id) throws Exception;//修改记录01
	public abstract List<idcard01> query_ID(int sign,String id) throws Exception;//查询ID记录
	public abstract List<idcard01> query_NAME(int sign,String name) throws Exception;//查询name记录
	public abstract List<idcard01> recycle_query_ID(int sign,String id) throws Exception;//回收站查询ID记录
	public abstract List<idcard01> recycle_query_NAME(int sign,String name) throws Exception;//回收站查询name记录
	public abstract List<idcard01> recycle_query_ALL(int sign) throws Exception;//回收站查询全部记录
	public abstract int recycle_delete_ALL(int sign) throws Exception;//回收站全部删除记录
	public abstract int recycle_reduction_ALL(int sign) throws Exception;//回收站全部还原记录
	public abstract int recycle_delete_Single(int sign,int id) throws Exception;//回收站单个删除记录
	public abstract int recycle_reduction_Single(int sign,int id) throws Exception;//回收站单个还原记录
}
