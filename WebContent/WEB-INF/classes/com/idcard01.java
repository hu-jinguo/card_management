package com;

public class idcard01 {
	private int sign=0;
	private int id=0;
	private String name;
	private String sex;
	private String email;
	private int recycle;
	public int getSign() {
		return sign;
	}
	public void setSign(int sign) {
		this.sign = sign;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getRecycle() {
		return recycle;
	}
	public void setRecycle(int recycle) {
		this.recycle = recycle;
	}
	public idcard01(){}
	public idcard01(int sign01,int id01,String name01,String sex01,String email01,int recycle01)
	{
		sign=sign01;
		id=id01;
		name=name01;
		sex=sex01;
		email=email01;
		recycle=recycle01;
	}
}
