<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>普通用户名片管理系统</title>
    <style>
     .nav{
         position:absolute;
         top:0px;
         left:0px;
         right:0px;
         background-color:rgb(60,141,188);
         height:60px;
         width:100%;
         display:center;
         text-align:left;
         line-height:60px;
        }
        .left-nav{
        position:absolute;
        top:60px;
        left:0px;
        background-color:rgb(15,41,80);
        width:200px;
        height:1400px;
        }
        .list li{
        padding-top:10px;
        padding-bottom:10px;
        }
         .list li a{
	    /* 去掉下划线 */
	    text-decoration: none;
        }
        .nav1{
        position:absolute;
        top:0px;
        left:200px;
        width:1224px;
        height:50px;
        background-color:rgb(250,250,250);
        display:center;
        line-height:50px;
        }
        .intro{
        position:absolute;
        top:100px;
        left:220px;
        width:500px;
        height:600px
        }
    </style>
</head>
<body>
<!--导航条-->
<div class="nav">
        <span style="display:inline-block;text-align:center;width:200px;font-weight:bold;"><font color="white">名片管理系统（普通用户）</font></span>
</div>
<!--左侧导航栏-->
<div class="left-nav">
        <div class="nav1">
            <span style="display:inline-block;width:80px;text-align:center;font-weight:bold;"><font color="black">首页</font></span>
        </div>
    <span class="intro">
        <center>
		  <iframe name="mainFrame1" style="width: 1200px; height: 500px;" noresize="noresize" frameborder="0">
		</iframe>
	   </center>  
    </span>
    
    <ul class="list">
        <li><a href="insert01.jsp" target="mainFrame1"><h3><font color="grey">添加记录</font></h3></a></li>
        <li><a href="update01.jsp" target="mainFrame1"><h3><font color="grey">修改记录</font></h3></a></li>
        <li><a href="delete01.jsp" target="mainFrame1"><h3><font color="grey">删除记录</font></h3></a></li>
        <li><a href="query01.jsp" target="mainFrame1"><h3><font color="grey">查询记录</font></h3></a></li>
        <li><a href="recycle01.jsp" target="mainFrame1"><h3><font color="grey">回收站记录</font></h3></a></li>
        <li><a href="daorudaochu.jsp" target="mainFrame1"><h3><font color="grey">导入导出</font></h3></a></li>
    </ul>
</div>
</body>
</html>
