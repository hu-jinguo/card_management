<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>通用户添加验证操作页面</title>
</head>
<body>
     用户添加页面：<hr width="100%" size="3"><br><br><br>
   <%request.setCharacterEncoding("UTF-8");%>
    <jsp:useBean id="idcardDAO" class="com.idcardDAO"></jsp:useBean>
    <jsp:useBean id="idcard01" class="com.idcard01"></jsp:useBean>
    <%
    boolean flag=idcardDAO.query((int)session.getAttribute("super_insert_sign"), Integer.parseInt(request.getParameter("super_insert_id01")));
    if(flag)
    {
    	%>
		<script type="text/javascript">
		  alert("该用户ID已存在！");
		  window.history.back(-1);
		</script>
		<%
    }
    else
    {
    	idcard01.setSign((int)session.getAttribute("super_insert_sign"));
    	idcard01.setId(Integer.parseInt(request.getParameter("super_insert_id01")));
    	idcard01.setName(request.getParameter("super_insert_name02"));
    	idcard01.setSex(request.getParameter("super_insert_sex01"));
    	idcard01.setEmail(request.getParameter("super_insert_email01"));
    	idcard01.setRecycle(0);
    	int n=idcardDAO.insert(idcard01);   	
    	if(n!=0)
    	{
    		out.println("添加成功！");
    		response.setHeader("refresh", "2;url=super_insert01.jsp");
    	}
    	else
    	{
    		%>
    		<script type="text/javascript">
    		  alert("添加失败！");
    		  window.history.back(-1);
    		</script>
    		<%
    	}
    }
	%>
</body>
</html>