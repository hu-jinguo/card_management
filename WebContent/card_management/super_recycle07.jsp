<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员回收站页面</title>
</head>
<body>
     回收站验证页面：<hr width="100%" size="3"><br><br><br>
  <%request.setCharacterEncoding("UTF-8");%>
   <jsp:useBean id="DBconnect" class="com.DBconnect"></jsp:useBean>
   <%
    Connection conn=DBconnect.getDBconnect();
	String bp=(String)session.getAttribute("bp1");
	if(bp.equals("1"))
	{
		String sql2="delete from idcard where id=? and sign=?";
		PreparedStatement pst2=conn.prepareStatement(sql2);
		pst2.setInt(1, (int)session.getAttribute("super_recycle_id02"));
		pst2.setInt(2, (int)session.getAttribute("super_recycle_sign02"));
		int n2=pst2.executeUpdate();
		if(n2!=0)
		{
			out.println("删除成功！");
		}
		else
		{
			out.println("删除失败！");
		}
		if(pst2!=null) pst2.close();
	}
	else if(bp.equals("0"))
	{
		String sql1="select * from iduser where recycle=0 and sign=?";
		PreparedStatement pst1=conn.prepareStatement(sql1);
		pst1.setInt(1, (int)session.getAttribute("super_recycle_sign02"));
		ResultSet rs1=pst1.executeQuery();
		if(rs1.next())
		{
			String sqlx="update idcard set recycle=0 where id=? and sign=?";
			PreparedStatement pstx=conn.prepareStatement(sqlx);
			pstx.setInt(1, (int)session.getAttribute("super_recycle_id02"));
			pstx.setInt(2, (int)session.getAttribute("super_recycle_sign02"));
			int n1=pstx.executeUpdate();
			if(n1!=0)
			{
				out.println("还原成功！");
			}
			else
			{
				out.println("还原失败！");
			}
			if(pstx!=null) pstx.close();
		}
		else
		{
			%>
			<script type="text/javascript">
			  alert("该名片所有者不存在！");
			</script>
			<%
		}
		if(rs1!=null) rs1.close();
		if(pst1!=null) pst1.close();
	}
	if(conn!=null) conn.close();
	
	%>
</body>
</html>