<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>普通用户删除验证页面</title>
</head>
<body>
     用户删除信息页面：<hr width="100%" size="3"><br><br><br>
    <%request.setCharacterEncoding("UTF-8");%>
    <jsp:useBean id="idcardDAO" class="com.idcardDAO"></jsp:useBean>
    <jsp:useBean id="idcard01" class="com.idcard01"></jsp:useBean>
    <%
    boolean flag=idcardDAO.query((int)session.getAttribute("super_delete_sign"), Integer.parseInt(request.getParameter("super_delete_id01")));
	if(flag)
	{
		int n=idcardDAO.update02((int)session.getAttribute("super_delete_sign"), Integer.parseInt(request.getParameter("super_delete_id01")));
		if(n!=0)
		{
			out.println("删除一条记录成功！");
		}
		else
		{
			out.println("删除失败！");
		}
	}
	else
	{
		%>
		<script type="text/javascript">
		  alert("没有该用户！");
		  window.history.back(-1);
		</script>
		<%
	}
	%>
	
</body>
</html>