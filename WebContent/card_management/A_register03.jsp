<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>普通用户注册账号密码验证页面</title>
</head>
<body>
	 <%request.setCharacterEncoding("UTF-8");%>
	  <jsp:useBean id="ord_register01" class="com.ord_register01"></jsp:useBean>
	  <jsp:useBean id="ord_register02" class="com.ord_register02"></jsp:useBean>
	  <jsp:useBean id="ord_register03" class="com.ord_register03"></jsp:useBean>
	  <%
	  if(ord_register01.pdUser(request.getParameter("name01"), request.getParameter("root01")))
	  {
		     %>
		     <script type="text/javascript">
		       alert("该用户已存在，去登陆吧！");
		     </script>
		     <%
		  response.setHeader("refresh", "1;url=A_register02.html");
	  }
	  else{
		  //产生随机标志
		  int max=1000,min=1;
		  long rd = System.currentTimeMillis();//时间种子
		  int x = (int) (rd%(max-min)+min);//随机数1~1000

		  while(true)
		  {
			  if(ord_register02.pdUser(x))
			  {
				  x+=1;
				  if(x>1000) x=0;
			  }
			  else
			  {
				  break;
			  }
		  }
		  ord_register03.setSign(x);
		  int signx=ord_register03.getSign();
		  if(ord_register03.pdUser(signx, request.getParameter("name01"), 
				  request.getParameter("root01"), request.getParameter("password01"))!=0){
			     %>
			     <script type="text/javascript">
			       alert("注册成功！");
			     </script>
			     <%
		    	 response.setHeader("refresh", "1;url=A_register02.html");
		     }else{
		    	 %>
		    	 <script type="text/javascript">
			       alert("注册失败！");
			     </script>
		    	 <%
		     }  
	  }
	  %>
	  
</body>
</html>