<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员回收站页面</title>
</head>
<body>
     回收站验证页面：<hr width="100%" size="3"><br><br><br>
   <%request.setCharacterEncoding("UTF-8");%>
   <jsp:useBean id="DBconnect" class="com.DBconnect"></jsp:useBean>
   <%
    Connection conn=DBconnect.getDBconnect();
	String bp=(String)session.getAttribute("bp2");
	if(bp.equals("1"))
	{
		String sql2="delete from iduser where name=?";
		PreparedStatement pst2=conn.prepareStatement(sql2);
		pst2.setString(1, (String)session.getAttribute("super_recycle_name03"));
		int n2=pst2.executeUpdate();
		if(n2!=0)
		{
			out.println("删除成功！");
		}
		else
		{
			out.println("删除失败！");
		}
		if(pst2!=null) pst2.close();
	}
	else if(bp.equals("0"))
	{
		String sql1="update iduser set recycle=0 where name=?";
		PreparedStatement pst1=conn.prepareStatement(sql1);
		pst1.setString(1, (String)session.getAttribute("super_recycle_name03"));
		int n1=pst1.executeUpdate();
		if(n1!=0)
		{
			out.println("还原成功！");
		}
		else
		{
			out.println("还原失败！");
		}
		if(pst1!=null) pst1.close();
			
	}
	
	%>
</body>
</html>